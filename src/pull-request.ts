import {Bitbucket} from 'bitbucket';
import git from 'nodegit';

import {fetchCredentials} from './credentials';

async function getBranch(): Promise<string> {
  const repo = await git.Repository.open('./');
  const current = await repo.getCurrentBranch();

  return current.shorthand();
}

function isBranchProtected(branchName: string): boolean {
  return ['develop', 'master'].includes(branchName);
}

async function getBBRemote(): Promise<string | undefined> {
  const repo = await git.Repository.open('./');
  const remotes = await repo.getRemotes();

  return remotes.find(remote => remote.url().includes('bitbucket.org'))?.url();
}

interface PROptions {
  title: string;
  summary?: string;
  destinationBranch?: string;
}

export async function createPullRequest({
  title,
  summary,
  destinationBranch = 'develop'
}: PROptions) {
  const bb = new Bitbucket({
    auth: await fetchCredentials()
  });

  const branchName = await getBranch();
  const remote = await getBBRemote();

  if (!remote) {
    console.error('No bitbucket remote found.');
    return;
  }

  if (isBranchProtected(branchName)) {
    console.error("Don't use this to make a PR from develop/master");
    return;
  }

  const m = remote.match(/bitbucket\.org:(.*)\/(.*)\.git/);

  if (!m) {
    console.error('Unable to find org & project');
    return;
  }

  const [, org, project] = m;

  const {data} = await bb.pullrequests.list({
    repo_slug: project,
    workspace: org,
    state: 'OPEN'
  });

  const existingPR = data.values?.find(pr => {
    return pr.source?.branch?.name === branchName;
  });

  if (existingPR) {
    const prUrl = existingPR.links?.html?.href;
    console.error(
      `An existing, opened PR already exists for this branch: ${prUrl}`
    );
    return;
  }

  const {data: prDetails} = await bb.pullrequests.create({
    repo_slug: project,
    workspace: org,
    _body: {
      type: '',
      close_source_branch: true,
      source: {
        branch: {
          name: branchName
        }
      },
      destination: {
        branch: {
          name: destinationBranch
        }
      },
      title,
      description: summary
    }
  });

  console.log('Pull Request Created:');
  console.log(prDetails.links?.html?.href);
}
