import inquirer from 'inquirer';
import * as keytar from 'keytar';

const SERVICE_NAME = 'bitbucket-cli';

export async function resetCredentials() {
    const creds = await keytar.findCredentials(SERVICE_NAME);

    for (const cred of creds) {
      await keytar.deletePassword(SERVICE_NAME, cred.account);
    }
}

export async function fetchCredentials(
  reset = false
): Promise<{
  username: string;
  password: string;
}> {
  const creds = await keytar.findCredentials(SERVICE_NAME);

  if (creds.length === 0) {
    const out = await inquirer.prompt([
      {
        type: 'input',
        name: 'username',
        message: 'Username: '
      },
      {
        type: 'password',
        name: 'password',
        message: 'Password: '
      }
    ]);

    keytar.setPassword(SERVICE_NAME, out.username, out.password);

    return {username: out.username, password: out.password};
  } else {
    return {
      username: creds[0].account,
      password: creds[0].password
    };
  }
}