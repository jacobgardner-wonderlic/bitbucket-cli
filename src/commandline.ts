import yargs from 'yargs';

interface BaseOptions {
  resetCredentials?: boolean;
}

interface PullRequestOptions extends BaseOptions {
  command: 'pull-request';
  title: string;
  summary?: string;
  destinationBranch?: string;
}

interface NoCommandOptions extends BaseOptions {
  command: 'none';
}

type CLIOptions = NoCommandOptions | PullRequestOptions;

export function parseCLI(): Promise<CLIOptions> {
  return new Promise(resolve => {
    const argv: any = yargs
      .scriptName('bb')
      .usage('$0 [-r] <cmd> [args]')
      .option('reset', {
        type: 'boolean',
        alias: 'r',
        describe: 'Reset stored credentials'
      })
      .command(
        'pr',
        'Create a pull request for the current branch',
        yargs => {
          yargs.option('title', {
            type: 'string',
            alias: 't',
            describe: 'PR title',
            demand: true
          });

          yargs.option('summary', {
            type: 'string',
            alias: 's',
            describe: 'Summary of PR',
            demand: true
          });

          yargs.option('destinationBranch', {
            type: 'string',
            alias: 'd',
            describe: 'Branch to make PR against',
            default: 'develop'
          });
        },
        () => {
          return resolve({
            command: 'pull-request',
            ...argv
          });
        }
      )
      .help().argv;

    return resolve({
      command: 'none',
      ...argv
    });
  });
}
