#!/usr/bin/env node

import {parseCLI} from './commandline';
import {createPullRequest} from './pull-request';
import {resetCredentials} from './credentials';

async function main() {
  const cliOptions = await parseCLI();

  if (cliOptions.resetCredentials) {
    await resetCredentials();
  }

  if (cliOptions.command === 'pull-request') {
    await createPullRequest(cliOptions);
  }
}

main()
  .catch(console.error)
  .finally(() => console.log('Done'));
