BitBucket CLI for Jake
======================

Really just for my use-cases where I was annoyed with the UI.  So this is not useful in any broad sense

## For Installation

  ```
  yarn
  yarn link
  ```

## For development

  ```
  yarn
  yarn link
  yarn watch 
  ```

## Usage

  ```
  # Run in git directory with bitbucket as one of the origins
  bb pr 
    -t "Pull Request Title" 
    -s "Description here which might support markdown?"
    -d "Destination branch which defaults to develop"
  ```

The first time you use it, you'll be asked for username/password. Highly recommend using an app password with restricted permissions. Your username/password will be stored in your OS's keychain (haven't tested across many OSs). Use `bb -r` to reset your credentials.

https://i.imgur.com/saRMeu5.png
